<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Abbreviations Controller
 *
 * @property \App\Model\Table\AbbreviationsTable $Abbreviations
 *
 * @method \App\Model\Entity\Abbreviation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AbbreviationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $abbreviations = $this->paginate($this->Abbreviations);

        $this->set(compact('abbreviations'));
    }

    /**
     * View method
     *
     * @param string|null $id Abbreviation id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $abbreviation = $this->Abbreviations->get($id, [
            'contain' => ['Publications']
        ]);

        $this->set('abbreviation', $abbreviation);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $abbreviation = $this->Abbreviations->newEntity();
        if ($this->request->is('post')) {
            $abbreviation = $this->Abbreviations->patchEntity($abbreviation, $this->request->getData());
            if ($this->Abbreviations->save($abbreviation)) {
                $this->Flash->success(__('The abbreviation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The abbreviation could not be saved. Please, try again.'));
        }
        $this->set(compact('abbreviation'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Abbreviation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $abbreviation = $this->Abbreviations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $abbreviation = $this->Abbreviations->patchEntity($abbreviation, $this->request->getData());
            if ($this->Abbreviations->save($abbreviation)) {
                $this->Flash->success(__('The abbreviation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The abbreviation could not be saved. Please, try again.'));
        }
        $this->set(compact('abbreviation'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Abbreviation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $abbreviation = $this->Abbreviations->get($id);
        if ($this->Abbreviations->delete($abbreviation)) {
            $this->Flash->success(__('The abbreviation has been deleted.'));
        } else {
            $this->Flash->error(__('The abbreviation could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
