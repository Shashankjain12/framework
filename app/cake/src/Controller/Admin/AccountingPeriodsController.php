<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * AccountingPeriods Controller
 *
 * @property \App\Model\Table\AccountingPeriodsTable $AccountingPeriods
 *
 * @method \App\Model\Entity\AccountingPeriod[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AccountingPeriodsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Rulers', 'Months', 'Years']
        ];
        $accountingPeriods = $this->paginate($this->AccountingPeriods);

        $this->set(compact('accountingPeriods'));
    }

    /**
     * View method
     *
     * @param string|null $id Accounting Period id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $accountingPeriod = $this->AccountingPeriods->get($id, [
            'contain' => ['Rulers', 'Months', 'Years']
        ]);

        $this->set('accountingPeriod', $accountingPeriod);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $accountingPeriod = $this->AccountingPeriods->newEntity();
        if ($this->request->is('post')) {
            $accountingPeriod = $this->AccountingPeriods->patchEntity($accountingPeriod, $this->request->getData());
            if ($this->AccountingPeriods->save($accountingPeriod)) {
                $this->Flash->success(__('The accounting period has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The accounting period could not be saved. Please, try again.'));
        }
        $rulers = $this->AccountingPeriods->Rulers->find('list', ['limit' => 200]);
        $months = $this->AccountingPeriods->Months->find('list', ['limit' => 200]);
        $years = $this->AccountingPeriods->Years->find('list', ['limit' => 200]);
        $this->set(compact('accountingPeriod', 'rulers', 'months', 'years'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Accounting Period id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $accountingPeriod = $this->AccountingPeriods->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $accountingPeriod = $this->AccountingPeriods->patchEntity($accountingPeriod, $this->request->getData());
            if ($this->AccountingPeriods->save($accountingPeriod)) {
                $this->Flash->success(__('The accounting period has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The accounting period could not be saved. Please, try again.'));
        }
        $rulers = $this->AccountingPeriods->Rulers->find('list', ['limit' => 200]);
        $months = $this->AccountingPeriods->Months->find('list', ['limit' => 200]);
        $years = $this->AccountingPeriods->Years->find('list', ['limit' => 200]);
        $this->set(compact('accountingPeriod', 'rulers', 'months', 'years'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Accounting Period id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $accountingPeriod = $this->AccountingPeriods->get($id);
        if ($this->AccountingPeriods->delete($accountingPeriod)) {
            $this->Flash->success(__('The accounting period has been deleted.'));
        } else {
            $this->Flash->error(__('The accounting period could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
