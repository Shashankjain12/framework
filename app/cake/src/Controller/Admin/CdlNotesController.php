<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * CdlNotes Controller
 *
 * @property \App\Model\Table\CdlNotesTable $CdlNotes
 *
 * @method \App\Model\Entity\CdlNote[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CdlNotesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Authors']
        ];
        $cdlNotes = $this->paginate($this->CdlNotes);

        $this->set(compact('cdlNotes'));
    }

    /**
     * View method
     *
     * @param string|null $id Cdl Note id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cdlNote = $this->CdlNotes->get($id, [
            'contain' => ['Authors']
        ]);

        $this->set('cdlNote', $cdlNote);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $cdlNote = $this->CdlNotes->newEntity();
        if ($this->request->is('post')) {
            $cdlNote = $this->CdlNotes->patchEntity($cdlNote, $this->request->getData());
            if ($this->CdlNotes->save($cdlNote)) {
                $this->Flash->success(__('The cdl note has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cdl note could not be saved. Please, try again.'));
        }
        $authors = $this->CdlNotes->Authors->find('list', ['limit' => 200]);
        $this->set(compact('cdlNote', 'authors'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Cdl Note id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cdlNote = $this->CdlNotes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cdlNote = $this->CdlNotes->patchEntity($cdlNote, $this->request->getData());
            if ($this->CdlNotes->save($cdlNote)) {
                $this->Flash->success(__('The cdl note has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cdl note could not be saved. Please, try again.'));
        }
        $authors = $this->CdlNotes->Authors->find('list', ['limit' => 200]);
        $this->set(compact('cdlNote', 'authors'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Cdl Note id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cdlNote = $this->CdlNotes->get($id);
        if ($this->CdlNotes->delete($cdlNote)) {
            $this->Flash->success(__('The cdl note has been deleted.'));
        } else {
            $this->Flash->error(__('The cdl note could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
