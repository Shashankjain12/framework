<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * ExternalResources Controller
 *
 * @property \App\Model\Table\ExternalResourcesTable $ExternalResources
 *
 * @method \App\Model\Entity\ExternalResource[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ExternalResourcesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $externalResources = $this->paginate($this->ExternalResources);

        $this->set(compact('externalResources'));
    }

    /**
     * View method
     *
     * @param string|null $id External Resource id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $externalResource = $this->ExternalResources->get($id, [
            'contain' => ['Artifacts']
        ]);

        $this->set('externalResource', $externalResource);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $externalResource = $this->ExternalResources->newEntity();
        if ($this->request->is('post')) {
            $externalResource = $this->ExternalResources->patchEntity($externalResource, $this->request->getData());
            if ($this->ExternalResources->save($externalResource)) {
                $this->Flash->success(__('The external resource has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The external resource could not be saved. Please, try again.'));
        }
        $artifacts = $this->ExternalResources->Artifacts->find('list', ['limit' => 200]);
        $this->set(compact('externalResource', 'artifacts'));
    }

    /**
     * Edit method
     *
     * @param string|null $id External Resource id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $externalResource = $this->ExternalResources->get($id, [
            'contain' => ['Artifacts']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $externalResource = $this->ExternalResources->patchEntity($externalResource, $this->request->getData());
            if ($this->ExternalResources->save($externalResource)) {
                $this->Flash->success(__('The external resource has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The external resource could not be saved. Please, try again.'));
        }
        $artifacts = $this->ExternalResources->Artifacts->find('list', ['limit' => 200]);
        $this->set(compact('externalResource', 'artifacts'));
    }

    /**
     * Delete method
     *
     * @param string|null $id External Resource id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $externalResource = $this->ExternalResources->get($id);
        if ($this->ExternalResources->delete($externalResource)) {
            $this->Flash->success(__('The external resource has been deleted.'));
        } else {
            $this->Flash->error(__('The external resource could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
