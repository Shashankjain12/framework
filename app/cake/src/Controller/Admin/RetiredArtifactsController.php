<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * RetiredArtifacts Controller
 *
 * @property \App\Model\Table\RetiredArtifactsTable $RetiredArtifacts
 *
 * @method \App\Model\Entity\RetiredArtifact[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RetiredArtifactsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Artifacts', 'NewArtifacts']
        ];
        $retiredArtifacts = $this->paginate($this->RetiredArtifacts);

        $this->set(compact('retiredArtifacts'));
    }

    /**
     * View method
     *
     * @param string|null $id Retired Artifact id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $retiredArtifact = $this->RetiredArtifacts->get($id, [
            'contain' => ['Artifacts', 'NewArtifacts']
        ]);

        $this->set('retiredArtifact', $retiredArtifact);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $retiredArtifact = $this->RetiredArtifacts->newEntity();
        if ($this->request->is('post')) {
            $retiredArtifact = $this->RetiredArtifacts->patchEntity($retiredArtifact, $this->request->getData());
            if ($this->RetiredArtifacts->save($retiredArtifact)) {
                $this->Flash->success(__('The retired artifact has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The retired artifact could not be saved. Please, try again.'));
        }
        $artifacts = $this->RetiredArtifacts->Artifacts->find('list', ['limit' => 200]);
        $newArtifacts = $this->RetiredArtifacts->NewArtifacts->find('list', ['limit' => 200]);
        $this->set(compact('retiredArtifact', 'artifacts', 'newArtifacts'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Retired Artifact id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $retiredArtifact = $this->RetiredArtifacts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $retiredArtifact = $this->RetiredArtifacts->patchEntity($retiredArtifact, $this->request->getData());
            if ($this->RetiredArtifacts->save($retiredArtifact)) {
                $this->Flash->success(__('The retired artifact has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The retired artifact could not be saved. Please, try again.'));
        }
        $artifacts = $this->RetiredArtifacts->Artifacts->find('list', ['limit' => 200]);
        $newArtifacts = $this->RetiredArtifacts->NewArtifacts->find('list', ['limit' => 200]);
        $this->set(compact('retiredArtifact', 'artifacts', 'newArtifacts'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Retired Artifact id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $retiredArtifact = $this->RetiredArtifacts->get($id);
        if ($this->RetiredArtifacts->delete($retiredArtifact)) {
            $this->Flash->success(__('The retired artifact has been deleted.'));
        } else {
            $this->Flash->error(__('The retired artifact could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
