<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ArtifactsExternalResources Controller
 *
 * @property \App\Model\Table\ArtifactsExternalResourcesTable $ArtifactsExternalResources
 *
 * @method \App\Model\Entity\ArtifactsExternalResource[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactsExternalResourcesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Artifacts', 'ExternalResources']
        ];
        $artifactsExternalResources = $this->paginate($this->ArtifactsExternalResources);

        $this->set(compact('artifactsExternalResources'));
    }

    /**
     * View method
     *
     * @param string|null $id Artifacts External Resource id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artifactsExternalResource = $this->ArtifactsExternalResources->get($id, [
            'contain' => ['Artifacts', 'ExternalResources']
        ]);

        $this->set('artifactsExternalResource', $artifactsExternalResource);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $artifactsExternalResource = $this->ArtifactsExternalResources->newEntity();
        if ($this->request->is('post')) {
            $artifactsExternalResource = $this->ArtifactsExternalResources->patchEntity($artifactsExternalResource, $this->request->getData());
            if ($this->ArtifactsExternalResources->save($artifactsExternalResource)) {
                $this->Flash->success(__('The artifacts external resource has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts external resource could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsExternalResources->Artifacts->find('list', ['limit' => 200]);
        $externalResources = $this->ArtifactsExternalResources->ExternalResources->find('list', ['limit' => 200]);
        $this->set(compact('artifactsExternalResource', 'artifacts', 'externalResources'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Artifacts External Resource id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $artifactsExternalResource = $this->ArtifactsExternalResources->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $artifactsExternalResource = $this->ArtifactsExternalResources->patchEntity($artifactsExternalResource, $this->request->getData());
            if ($this->ArtifactsExternalResources->save($artifactsExternalResource)) {
                $this->Flash->success(__('The artifacts external resource has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts external resource could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsExternalResources->Artifacts->find('list', ['limit' => 200]);
        $externalResources = $this->ArtifactsExternalResources->ExternalResources->find('list', ['limit' => 200]);
        $this->set(compact('artifactsExternalResource', 'artifacts', 'externalResources'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Artifacts External Resource id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $artifactsExternalResource = $this->ArtifactsExternalResources->get($id);
        if ($this->ArtifactsExternalResources->delete($artifactsExternalResource)) {
            $this->Flash->success(__('The artifacts external resource has been deleted.'));
        } else {
            $this->Flash->error(__('The artifacts external resource could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
