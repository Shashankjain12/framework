<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArtifactDate Entity
 *
 * @property int $id
 * @property int $artifact_id
 * @property int $date_id
 *
 * @property \App\Model\Entity\Artifact $artifact
 * @property \App\Model\Entity\Date $date
 */
class ArtifactDate extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'date_id' => true,
        'artifact' => true,
        'date' => true
    ];
}
