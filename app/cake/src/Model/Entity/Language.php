<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Language Entity
 *
 * @property int $id
 * @property int|null $sequence
 * @property int|null $parent_id
 * @property string $language
 * @property string|null $protocol_code
 * @property string|null $inline_code
 * @property string|null $notes
 *
 * @property \App\Model\Entity\Language $parent_language
 * @property \App\Model\Entity\Language[] $child_languages
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class Language extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'sequence' => true,
        'parent_id' => true,
        'language' => true,
        'protocol_code' => true,
        'inline_code' => true,
        'notes' => true,
        'parent_language' => true,
        'child_languages' => true,
        'artifacts' => true
    ];
}
