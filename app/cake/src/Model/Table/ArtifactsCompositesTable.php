<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ArtifactsComposites Model
 *
 * @property \App\Model\Table\ArtifactsTable|\Cake\ORM\Association\BelongsTo $Artifacts
 *
 * @method \App\Model\Entity\ArtifactsComposite get($primaryKey, $options = [])
 * @method \App\Model\Entity\ArtifactsComposite newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ArtifactsComposite[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsComposite|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsComposite|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsComposite patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsComposite[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsComposite findOrCreate($search, callable $callback = null, $options = [])
 */
class ArtifactsCompositesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('artifacts_composites');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Artifacts', [
            'foreignKey' => 'artifact_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('composite')
            ->maxLength('composite', 10)
            ->allowEmpty('composite');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['artifact_id'], 'Artifacts'));

        return $rules;
    }
}
