<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CdlNotes Model
 *
 * @property \App\Model\Table\AuthorsTable|\Cake\ORM\Association\BelongsTo $Authors
 *
 * @method \App\Model\Entity\CdlNote get($primaryKey, $options = [])
 * @method \App\Model\Entity\CdlNote newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CdlNote[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CdlNote|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CdlNote|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CdlNote patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CdlNote[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CdlNote findOrCreate($search, callable $callback = null, $options = [])
 */
class CdlNotesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cdl_notes');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->belongsTo('Authors', [
            'foreignKey' => 'author_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('year')
            ->allowEmpty('year');

        $validator
            ->integer('number')
            ->allowEmpty('number');

        $validator
            ->scalar('title')
            ->allowEmpty('title');

        $validator
            ->scalar('text')
            ->maxLength('text', 4294967295)
            ->allowEmpty('text');

        $validator
            ->scalar('footnotes')
            ->maxLength('footnotes', 16777215)
            ->allowEmpty('footnotes');

        $validator
            ->scalar('bibliography')
            ->maxLength('bibliography', 16777215)
            ->allowEmpty('bibliography');

        $validator
            ->date('uploaded')
            ->allowEmpty('uploaded');

        $validator
            ->date('preprint')
            ->allowEmpty('preprint');

        $validator
            ->date('archival')
            ->allowEmpty('archival');

        $validator
            ->scalar('preprint_state')
            ->maxLength('preprint_state', 5)
            ->allowEmpty('preprint_state');

        $validator
            ->scalar('online')
            ->maxLength('online', 5)
            ->allowEmpty('online');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['author_id'], 'Authors'));

        return $rules;
    }
}
