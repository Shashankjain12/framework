<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\EntryType $entryType
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($entryType) ?>
            <legend class="capital-heading"><?= __('Add Entry Type') ?></legend>
            <?php
                echo $this->Form->control('label');
                echo $this->Form->control('author');
                echo $this->Form->control('title');
                echo $this->Form->control('journal');
                echo $this->Form->control('year');
                echo $this->Form->control('volume');
                echo $this->Form->control('pages');
                echo $this->Form->control('number');
                echo $this->Form->control('month');
                echo $this->Form->control('eid');
                echo $this->Form->control('note');
                echo $this->Form->control('crossref');
                echo $this->Form->control('keyword');
                echo $this->Form->control('doi');
                echo $this->Form->control('url');
                echo $this->Form->control('file');
                echo $this->Form->control('citeseerurl');
                echo $this->Form->control('pdf');
                echo $this->Form->control('abstract');
                echo $this->Form->control('comment');
                echo $this->Form->control('owner');
                echo $this->Form->control('timestamp');
                echo $this->Form->control('review');
                echo $this->Form->control('search');
                echo $this->Form->control('publisher');
                echo $this->Form->control('editor');
                echo $this->Form->control('series');
                echo $this->Form->control('address');
                echo $this->Form->control('edition');
                echo $this->Form->control('howpublished');
                echo $this->Form->control('lastchecked');
                echo $this->Form->control('booktitle');
                echo $this->Form->control('organization');
                echo $this->Form->control('language');
                echo $this->Form->control('chapter');
                echo $this->Form->control('type');
                echo $this->Form->control('school');
                echo $this->Form->control('nationality');
                echo $this->Form->control('yearfiled');
                echo $this->Form->control('assignee');
                echo $this->Form->control('day');
                echo $this->Form->control('dayfiled');
                echo $this->Form->control('monthfiled');
                echo $this->Form->control('institution');
                echo $this->Form->control('revision');
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('List Entry Types'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
