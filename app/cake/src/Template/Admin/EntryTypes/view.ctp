<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\EntryType $entryType
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('View Entry Type') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Label') ?></th>
                    <td><?= h($entryType->label) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Author') ?></th>
                    <td><?= h($entryType->author) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Title') ?></th>
                    <td><?= h($entryType->title) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Journal') ?></th>
                    <td><?= h($entryType->journal) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Year') ?></th>
                    <td><?= h($entryType->year) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Volume') ?></th>
                    <td><?= h($entryType->volume) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Pages') ?></th>
                    <td><?= h($entryType->pages) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Number') ?></th>
                    <td><?= h($entryType->number) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Month') ?></th>
                    <td><?= h($entryType->month) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Eid') ?></th>
                    <td><?= h($entryType->eid) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Note') ?></th>
                    <td><?= h($entryType->note) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Crossref') ?></th>
                    <td><?= h($entryType->crossref) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Keyword') ?></th>
                    <td><?= h($entryType->keyword) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Doi') ?></th>
                    <td><?= h($entryType->doi) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Url') ?></th>
                    <td><?= h($entryType->url) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('File') ?></th>
                    <td><?= h($entryType->file) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Citeseerurl') ?></th>
                    <td><?= h($entryType->citeseerurl) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Pdf') ?></th>
                    <td><?= h($entryType->pdf) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Abstract') ?></th>
                    <td><?= h($entryType->abstract) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Comment') ?></th>
                    <td><?= h($entryType->comment) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Owner') ?></th>
                    <td><?= h($entryType->owner) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Timestamp') ?></th>
                    <td><?= h($entryType->timestamp) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Review') ?></th>
                    <td><?= h($entryType->review) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Search') ?></th>
                    <td><?= h($entryType->search) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Publisher') ?></th>
                    <td><?= h($entryType->publisher) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Editor') ?></th>
                    <td><?= h($entryType->editor) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Series') ?></th>
                    <td><?= h($entryType->series) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Address') ?></th>
                    <td><?= h($entryType->address) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Edition') ?></th>
                    <td><?= h($entryType->edition) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Howpublished') ?></th>
                    <td><?= h($entryType->howpublished) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Lastchecked') ?></th>
                    <td><?= h($entryType->lastchecked) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Booktitle') ?></th>
                    <td><?= h($entryType->booktitle) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Organization') ?></th>
                    <td><?= h($entryType->organization) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Language') ?></th>
                    <td><?= h($entryType->language) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Chapter') ?></th>
                    <td><?= h($entryType->chapter) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Type') ?></th>
                    <td><?= h($entryType->type) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('School') ?></th>
                    <td><?= h($entryType->school) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Nationality') ?></th>
                    <td><?= h($entryType->nationality) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Yearfiled') ?></th>
                    <td><?= h($entryType->yearfiled) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Assignee') ?></th>
                    <td><?= h($entryType->assignee) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Day') ?></th>
                    <td><?= h($entryType->day) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Dayfiled') ?></th>
                    <td><?= h($entryType->dayfiled) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Monthfiled') ?></th>
                    <td><?= h($entryType->monthfiled) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Institution') ?></th>
                    <td><?= h($entryType->institution) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Revision') ?></th>
                    <td><?= h($entryType->revision) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($entryType->id) ?></td>
                </tr>
            </tbody>
        </table>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('Edit Entry Type'), ['action' => 'edit', $entryType->id], ['class' => 'btn-action']) ?>
        <?= $this->Form->postLink(__('Delete Entry Type'), ['action' => 'delete', $entryType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $entryType->id), 'class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Entry Types'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Entry Type'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>


<div class="boxed mx-0">
    <?php if (empty($entryType->publications)): ?>
        <div class="capital-heading"><?= __('No Related Publications') ?></div>
    <?php else: ?>
        <div class="capital-heading"><?= __('Related Publications') ?></div>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Bibtexkey') ?></th>
                <th scope="col"><?= __('Year') ?></th>
                <th scope="col"><?= __('Entry Type Id') ?></th>
                <th scope="col"><?= __('Address') ?></th>
                <th scope="col"><?= __('Annote') ?></th>
                <th scope="col"><?= __('Book Title') ?></th>
                <th scope="col"><?= __('Chapter') ?></th>
                <th scope="col"><?= __('Crossref') ?></th>
                <th scope="col"><?= __('Edition') ?></th>
                <th scope="col"><?= __('Editor') ?></th>
                <th scope="col"><?= __('How Published') ?></th>
                <th scope="col"><?= __('Institution') ?></th>
                <th scope="col"><?= __('Journal Id') ?></th>
                <th scope="col"><?= __('Month') ?></th>
                <th scope="col"><?= __('Note') ?></th>
                <th scope="col"><?= __('Number') ?></th>
                <th scope="col"><?= __('Organization') ?></th>
                <th scope="col"><?= __('Pages') ?></th>
                <th scope="col"><?= __('Publisher') ?></th>
                <th scope="col"><?= __('School') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Volume') ?></th>
                <th scope="col"><?= __('Publication History') ?></th>
                <th scope="col"><?= __('Abbreviation Id') ?></th>
                <th scope="col"><?= __('Series') ?></th>
                <th scope="col"><?= __('Oclc') ?></th>
                <th scope="col"><?= __('Designation') ?></th>
                <th scope="col"><?= __('Actions') ?></th>
            </thead>
            <tbody>
                <?php foreach ($entryType->publications as $publications): ?>
                <tr>
                    <td><?= h($publications->id) ?></td>
                    <td><?= h($publications->bibtexkey) ?></td>
                    <td><?= h($publications->year) ?></td>
                    <td><?= h($publications->entry_type_id) ?></td>
                    <td><?= h($publications->address) ?></td>
                    <td><?= h($publications->annote) ?></td>
                    <td><?= h($publications->book_title) ?></td>
                    <td><?= h($publications->chapter) ?></td>
                    <td><?= h($publications->crossref) ?></td>
                    <td><?= h($publications->edition) ?></td>
                    <td><?= h($publications->editor) ?></td>
                    <td><?= h($publications->how_published) ?></td>
                    <td><?= h($publications->institution) ?></td>
                    <td><?= h($publications->journal_id) ?></td>
                    <td><?= h($publications->month) ?></td>
                    <td><?= h($publications->note) ?></td>
                    <td><?= h($publications->number) ?></td>
                    <td><?= h($publications->organization) ?></td>
                    <td><?= h($publications->pages) ?></td>
                    <td><?= h($publications->publisher) ?></td>
                    <td><?= h($publications->school) ?></td>
                    <td><?= h($publications->title) ?></td>
                    <td><?= h($publications->volume) ?></td>
                    <td><?= h($publications->publication_history) ?></td>
                    <td><?= h($publications->abbreviation_id) ?></td>
                    <td><?= h($publications->series) ?></td>
                    <td><?= h($publications->oclc) ?></td>
                    <td><?= h($publications->designation) ?></td>
                    <td class="d-flex flex-row">
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                            ['controller' => 'Publications', 'action' => 'view', $publications->id],
                            ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                            ['controller' => 'Publications', 'action' => 'edit', $publications->id],
                            ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                        <?= $this->Form->postLink(
                            $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                            ['controller' => 'Publications', 'action' => 'delete', $publications->id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $publications->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>


