<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Inscription $inscription
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('View Inscription') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Artifact') ?></th>
                    <td><?= $inscription->has('artifact') ? $this->Html->link($inscription->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $inscription->artifact->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($inscription->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Created By') ?></th>
                    <td><?= $this->Number->format($inscription->created_by) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Credit To') ?></th>
                    <td><?= $this->Number->format($inscription->credit_to) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Created') ?></th>
                    <td><?= h($inscription->created) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Is Latest') ?></th>
                    <td><?= $inscription->is_latest ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Atf2conll Diff Resolved') ?></th>
                    <td><?= $inscription->atf2conll_diff_resolved ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Atf2conll Diff Unresolved') ?></th>
                    <td><?= $inscription->atf2conll_diff_unresolved ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Transliteration') ?></th>
                    <td><?= $this->Text->autoParagraph(h($inscription->transliteration)); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Transliteration Clean') ?></th>
                    <td><?= $this->Text->autoParagraph(h($inscription->transliteration_clean)); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Tranliteration Sign Names') ?></th>
                    <td><?= $this->Text->autoParagraph(h($inscription->tranliteration_sign_names)); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Annotation') ?></th>
                    <td><?= $this->Text->autoParagraph(h($inscription->annotation)); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Comments') ?></th>
                    <td><?= $this->Text->autoParagraph(h($inscription->comments)); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Structure') ?></th>
                    <td><?= $this->Text->autoParagraph(h($inscription->structure)); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Translations') ?></th>
                    <td><?= $this->Text->autoParagraph(h($inscription->translations)); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Transcriptions') ?></th>
                    <td><?= $this->Text->autoParagraph(h($inscription->transcriptions)); ?></td>
                </tr>
            </tbody>
        </table>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('Edit Inscription'), ['action' => 'edit', $inscription->id], ['class' => 'btn-action']) ?>
        <?= $this->Form->postLink(__('Delete Inscription'), ['action' => 'delete', $inscription->id], ['confirm' => __('Are you sure you want to delete # {0}?', $inscription->id), 'class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Inscriptions'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Inscription'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>



