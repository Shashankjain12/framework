<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */


?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $this->fetch('title') ?> - Cuneiform Digital Library Initiative
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('main.css')?>
    <?= $this->Html->css('font-awesome/css/font-awesome.min.css')?>

    <?= $this->Html->script('jquery.min.js')?>
    <?= $this->Html->script('bootstrap.min.js')?>
    <?= $this->Html->script('js.js')?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>

<body>
    <nav class="navbar navbar-expand navbar-light bg-light">
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mr-auto">
                <span class="text-secondary">Administrator</span>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item mx-3">
                    <a class="nav-link" href="#">Publications <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item mx-3">
                    <a class="nav-link" href="#">Resources</a>
                </li>
                <li class="nav-item mx-3">
                    <?= $this->Html->link("Login", ['controller' => 'users'], ['class' => 'nav-link']) ?>
                </li>
                <li class="nav-item mx-3 pr-5">
                    <?= $this->Html->link("Logout", ['controller' => 'logout'], ['class' => 'nav-link']) ?>
                </li>
            </ul>
        </div>
    </nav>

    <nav class="navbar navbar-expand-lg navbar-light bg-transparent py-0 my-5" id="navbar-main">
        <a class="navbar-brand logo py-0 ml-5" href="#">
            <div class="navbar-logo">
                <img src="/images/logo.png"/>
            </div>
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item mx-3 active">
                    <a class="nav-link" href="#">Browse</a>
                </li>
                <li class="nav-item mx-3">
                    <a class="nav-link" href="#">Contribute</a>
                </li>
                <li class="nav-item mx-3">
                    <a class="nav-link" href="#">About</a>
                </li>
                <li class="nav-item mx-3 mr-5 dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Search</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Advanced search</a>
                        <a class="dropdown-item" href="#">Search settings</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container-fluid text-center contentWrapper">
        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>
    </div>

    <footer>
        <div class="container-fluid">
            <div class="footerWrapper p-5">
                <div class="row">
                    <div class="col-md-4">
                        <ul class="footer-links">
                            <li class="footer-heading">SITEMAP</li>
                            <li><a href="#">Browse collection</a></li>
                            <li><a href="#">Contribute</a></li>
                            <li><a href="#">About CDLI</a></li>
                            <li><a href="#">Search collection</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul class="footer-links">
                            <li class="footer-heading">Acknowledgements</li>
                            <li><a href="#">Lorem Ipsum</a></li>
                            <li><a href="#">Lorem Ipsum</a></li>
                            <li><a href="#">Lorem Ipsum</a></li>
                            <li><a href="#">Lorem Ipsum</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 contact">
                        <ul class="footer-links">
                            <li class="footer-heading">Contact Us</li>
                            <li>123 Front St. W.</li>
                            <li>Toronto, CA</li>
                            <li>1A2 3R4</li>
                            <li class="my-3">(123)-456-7890</li>
                            <li class="my-3">contact@cdli.com</li>
                            <li>
                                <div class="d-flex">
                                    <div class="btn btn-dark rounded-circle mr-3">
                                        <a href="https://twitter.com/cdli_news">
                                            <i class="fa fa-twitter text-white" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <div class="footer-donate">
                                        <a href="#" class="btn btn-primary rounded-0">Donate</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer-end p-4">
            <div class="text-center text-white">
                © 2019 Cuneiform Digital Library Initiative.
            </div>
        </div>
    </footer>
</body>

</html>
