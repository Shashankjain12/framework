<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PostingType $postingType
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($postingType) ?>
            <legend class="capital-heading"><?= __('Edit Posting Type') ?></legend>
            <?php
                echo $this->Form->control('posting_type');
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $postingType->id],
                ['class' => 'btn-action'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $postingType->id)]
            )
        ?>
        <br/>
        <?= $this->Html->link(__('List Posting Types'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Postings'), ['controller' => 'Postings', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Posting'), ['controller' => 'Postings', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
