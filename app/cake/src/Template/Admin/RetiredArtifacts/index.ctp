<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\RetiredArtifact[]|\Cake\Collection\CollectionInterface $retiredArtifacts
 */
?>
<div class="boxed mx-0">
    <div class="capital-heading"><?= __('Related Actions') ?></div>

        <?= $this->Html->link(__('New Retired Artifact'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>

</div>

<h3 class="display-4 pt-3"><?= __('Retired Artifacts') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('new_artifact_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_public') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($retiredArtifacts as $retiredArtifact): ?>
        <tr>
            <td><?= $this->Number->format($retiredArtifact->id) ?></td>
            <td><?= $retiredArtifact->has('artifact') ? $this->Html->link($retiredArtifact->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $retiredArtifact->artifact->id]) : '' ?></td>
            <td><?= h($retiredArtifact->new_artifact_id) ?></td>
            <td><?= h($retiredArtifact->is_public) ?></td>
            <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                        ['action' => 'view', $retiredArtifact->id],
                        ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $retiredArtifact->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $retiredArtifact->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $retiredArtifact->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?= $this->Paginator->first() ?>
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next() ?>
        <?= $this->Paginator->last() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>

