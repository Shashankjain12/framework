<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactType $artifactType
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($artifactType) ?>
            <legend class="capital-heading"><?= __('Edit Artifact Type') ?></legend>
            <?php
                echo $this->Form->control('artifact_type');
                echo $this->Form->control('parent_id', ['options' => $parentArtifactTypes, 'empty' => true]);
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $artifactType->id],
                ['class' => 'btn-action'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $artifactType->id)]
            )
        ?>
        <br/>
        <?= $this->Html->link(__('List Artifact Types'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Parent Artifact Types'), ['controller' => 'ArtifactTypes', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Parent Artifact Type'), ['controller' => 'ArtifactTypes', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
