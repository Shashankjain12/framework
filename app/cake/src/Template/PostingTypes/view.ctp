<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PostingType $postingType
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('View Posting Type') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Posting Type') ?></th>
                    <td><?= h($postingType->posting_type) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($postingType->id) ?></td>
                </tr>
            </tbody>
        </table>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('Edit Posting Type'), ['action' => 'edit', $postingType->id], ['class' => 'btn-action']) ?>
        <?= $this->Form->postLink(__('Delete Posting Type'), ['action' => 'delete', $postingType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $postingType->id), 'class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Posting Types'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Posting Type'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Postings'), ['controller' => 'Postings', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Posting'), ['controller' => 'Postings', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>


<div class="boxed mx-0">
    <?php if (empty($postingType->postings)): ?>
        <div class="capital-heading"><?= __('No Related Postings') ?></div>
    <?php else: ?>
        <div class="capital-heading"><?= __('Related Postings') ?></div>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Posting Type Id') ?></th>
                <th scope="col"><?= __('Slug') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Published') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Body') ?></th>
                <th scope="col"><?= __('Lang') ?></th>
                <th scope="col"><?= __('Created By') ?></th>
                <th scope="col"><?= __('Modified By') ?></th>
                <th scope="col"><?= __('Publish Start') ?></th>
                <th scope="col"><?= __('Publish End') ?></th>
                <th scope="col"><?= __('Actions') ?></th>
            </thead>
            <tbody>
                <?php foreach ($postingType->postings as $postings): ?>
                <tr>
                    <td><?= h($postings->id) ?></td>
                    <td><?= h($postings->posting_type_id) ?></td>
                    <td><?= h($postings->slug) ?></td>
                    <td><?= h($postings->created) ?></td>
                    <td><?= h($postings->modified) ?></td>
                    <td><?= h($postings->published) ?></td>
                    <td><?= h($postings->title) ?></td>
                    <td><?= h($postings->body) ?></td>
                    <td><?= h($postings->lang) ?></td>
                    <td><?= h($postings->created_by) ?></td>
                    <td><?= h($postings->modified_by) ?></td>
                    <td><?= h($postings->publish_start) ?></td>
                    <td><?= h($postings->publish_end) ?></td>
                    <td class="d-flex flex-row">
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                            ['controller' => 'Postings', 'action' => 'view', $postings->id],
                            ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                            ['controller' => 'Postings', 'action' => 'edit', $postings->id],
                            ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                        <?= $this->Form->postLink(
                            $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                            ['controller' => 'Postings', 'action' => 'delete', $postings->id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $postings->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>


